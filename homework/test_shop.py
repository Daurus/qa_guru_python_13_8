
import pytest

from homework.models import Product, Cart


@pytest.fixture
def product():
    return Product("apple", 100, "This is a apple", 1000)

@pytest.fixture
def book():
    return Product("book", 100, "This is a book", 1000)

@pytest.fixture
def copybook():
    return Product("copybook", 50, "This is a copybook", 3000)

@pytest.fixture
def pencil():
    return Product("pencil", 25, "This is a pencil", 1000)



@pytest.fixture
def cart():
    cart = Cart()
    return cart

class TestProducts:

    def test_product_check_quantity(self, product):

        equal_quantity = product.quantity
        assert product.check_quantity(equal_quantity), 'not correct with quantity equal product.quantity'

        less_quantity = product.quantity - 1
        assert product.check_quantity(less_quantity), 'not correct with quantity less product.quantity'

        more_quantity = product.quantity + 1
        assert not product.check_quantity(more_quantity), 'not correct with quantity more than product.quantity'


    def test_product_buy(self, product):
        equal_quantity = product.quantity
        product.buy(equal_quantity)
        assert product.quantity == 0, 'failed with equal quantity'

        less_quantity = product.quantity - 1
        product.buy(less_quantity)
        assert product.quantity == 1, 'failed with less quantity'

    def test_product_buy_more_than_available(self, product):
        more_quantity = product.quantity + 1
        with pytest.raises(ValueError):
            assert product.buy(more_quantity) is ValueError


class TestCart:
    def test_add_product(self, book, cart):
        cart.add_product(book, quantity=5)
        assert cart.products[book] == 5, 'failed with adding to empty cart'

        cart.add_product(book, quantity=100)
        assert cart.products[book] == 105, 'failed with adding to product in cart'

    def test_remove_product(self, book, product, cart):
        cart.add_product(product, 5)
        cart.remove_product(product)
        assert product not in cart.products, 'not correct with quantity not recieved'

        cart.add_product(product, 1)
        cart.remove_product(product, 5000)
        assert product not in cart.products, 'not correct with quantity more than stock'

    def test_clear(self, product, copybook, pencil, cart):
        cart.add_product(copybook, 150)
        cart.add_product(pencil, 300)
        cart.clear()

        assert cart.products == {}, 'cart not empty'


    def test_get_total_price(self, product, book, copybook, pencil, cart):
        cart.add_product(product, 1)
        cart.add_product(copybook, 2)
        cart.add_product(pencil, 3)
        cart.add_product(book, 4)

        assert cart.get_total_price() == 675, 'Total price not correct'

    def test_buy(self, copybook, book, product, pencil, cart):
        cart.add_product(product, 1)
        cart.add_product(copybook, 10)
        cart.add_product(pencil, 4)
        cart.add_product(book, 4)
        cart.buy()

        assert cart.get_total_price() == float(0), 'buy() method give out not correct total_price'
        assert product.quantity == 999
        assert pencil.quantity == 996, 'product quantity not correct after buying cart'
        assert copybook.quantity == 2990, 'product quantity not correct after buying cart'

    def test_buy_more_than_stock(self, product, cart):
        cart.add_product(product, product.quantity + 1)
        with pytest.raises(ValueError):
             cart.buy()



